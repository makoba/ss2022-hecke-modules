# ss2022-hecke-modules

These are notes for a talk I am giving in the research seminar of the ESAGA (organized by Lennart Gehrmann).
A compiled version can be found [here](https://makoba.gitlab.io/ss2022-hecke-modules/hecke-modules.pdf).