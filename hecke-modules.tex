\documentclass[10pt, a4paper]{scrartcl}

\usepackage{packages}
\usepackage{commands}
\bibliography{references.bib}

\title{Hecke Modules and Eisenstein Ideals}
\author{Manuel Hoff}
\date{}

\begin{document}

    \maketitle

    These are notes for a talk I am giving in the research seminar of the ESAGA on \emph{The derived Hecke algebra for dihedral weight one forms}, organized by Lennart Gehrmann and taking place in the summer term 2022 in Essen.
    I would like to thank Lennart Gehrmann for helping me with the preparation of the talk.
    It is likely that there are mistakes in these notes, so use them at your own risk!

    The main reference for this talk is \cite{emerton}, but keep in mind that we work away from the primes $2$ and $3$ here so that some of the notation differs from the notation in the reference.

    \section*{Overview}

    The goal for the talk is the following:

    \begin{itemize}
        \item
        Recall some of the material from the previous talks and in particular the definitions of the modules $\calM$ and $\calX$ over the Hecke algebra $\TT$ as well as the symmetric $\TT$-bilinear pairing
        \[
            \calX \times \calX \to \calM.
        \]

        \item
        Show that $\calX$ and $\calM$ are invertible $\TT$-modules.

        \item
        Show that the pairing above gives an isomorphism $\calX \otimes_{\TT} \calX \cong \calM$.
    \end{itemize}
    Here the word \enquote{Show} has to be interpreted in a very weak sense as we will give almost no proofs.

    \section{\texorpdfstring{The Modular Curve $X_0(N)$}{The Modular Curve X0(N)}}

    In this subsection let $M \geq 1$ be an arbitrary integer.
    References for the definition of the modular curves are \cite{katz-mazur} and \cite{conrad}.

    \begin{itemize}
        \item
        A finite locally free subgroup $C \subseteq E$ of an elliptic curve $E$ over some ring $R$ is called \emph{cyclic of order $M$} if fppf-locally on $R$ there exists a point $P \in E(R)$ such that
        \[
            C = \sum_{i = 0}^{M - 1} [i P]
        \]
        as (relative) effective Cartier divisors on $E$.
        Let us make the following comments on this definition:
        \begin{itemize}
            \item
            If $M \in R^{\times}$ then $C \subseteq E$ is cyclic of order $M$ if and only if $C \cong \ZZ/M\ZZ$ fppf-locally on $R$.

            \item
            If $M$ is a prime, then every finite locally free subgroup $C \subseteq E$ of order $M$ is cyclic (of order $M$).

            \item
            In general the notion of a cyclic finite locally free subgroup of an elliptic curve is (at least to me) a bit counterintuitive:
            Let $M = p^2$ for a prime $p$ and let $E/k$ be an ordinary elliptic curve over some algebraically closed field $k$ of characteristic $p$.
            Then the $p$-torsion
            \[
                E[p] \cong (\ZZ/p\ZZ) \times \mu_p
            \]
            is cyclic of order $M$.
            The reason I find this counterintuitive is that I am not used to cyclic groups that decompose as a product!
        \end{itemize}

        \item
        $Y_0(M)/\ZZ$ denotes the \emph{(open) modular curve} of level $\Gamma_0(N)$.
        It is a coarse moduli space for the moduli problem
        \[
            R \mapsto \set[\Big]{(E, C)}{\text{$E/R$ elliptic curve, $C \subseteq E$ finite locally free subgroup that is cyclic of order $M$}}.
        \]
        The scheme $Y_0(M)$ naturally is an open subscheme of the \emph{(closed) modular curve} $X_0(M)$ that is itself a coarse moduli space for a certain moduli problem of generalized elliptic curves with extra structure.
        Inside $X_0(M)$ we have a (relative) effective Cartier divisor $\cusps$ and $Y_0(M) = X_0(M) \setminus \cusps$.
        We have the following properties:
        \begin{itemize}
            \item
            $X_0(M) \to \Spec(\ZZ)$ is fppf of relative dimension $1$ and proper with connected and reduced geometric fibers.

            \item
            $X_0(M)[M^{-1}]/\ZZ[M^{-1}]$ is smooth.
        \end{itemize}

        \item
        Suppose that $M$ is a prime.
        \begin{itemize}
            \item
            $X_0(M)$ has two cusps, i.e.\ $\deg(\cusps) = 2$.

            \item
            $X_0(M)_{\FMbar}$ has two irreducible components, both isomorphic to $\PP^1_{\FMbar}$, that intersect transversely in finitely many points.
            The divisor $\cusps_{\FMbar} \subseteq X_0(M)_{\FMbar}$ is the union of two nonsingular points, each one lying in one of the two irreducible components.

            \item
            Given a supersingular elliptic curve $E/\FMbar$ there exists a unique finite subgroup $C \subseteq E$ of order $M$ (and we have $C \cong \alpha_M$) the points $[(E, C)] \in X_0(M)(\FMbar)$ with $E$ supersingular are precisely the singular points of $X_0(M)_{\FMbar}$.
            Note that this in particular implies that the number of isomorphism classes of supersingular elliptic curves over $\FMbar$ is precisely $g(X_0(M)) + 1$.

            \item
            The two connected components of the nonsingular locus of $Y_0(M)_{\FMbar}$ can be described as the loci of those points $[(E, C)]$ such that $C$ is isomorphic to $\ZZ/M\ZZ$ and $\mu_M$ respectively.
        \end{itemize}

        \item
        We write $J_0(M)/\ZZ$ for the Néron model of the Jacobian of $X_0(M)_{\QQ}$.
    \end{itemize}


    \section{\texorpdfstring{The Module of Modular Forms $\calM$ and the Hecke Algebra $\TT$}{The Module of Modular Forms M and the Hecke Algebra T}}

    For the rest of the talk we fix a prime $N > 3$.
    We write $g = g(X_0(N))$ for the genus of the modular curve $X_0(N)$ and as in the last talk we write
    \[
        \frac{N - 1}{12} = \frac{\delta}{\Delta}
    \]
    with coprime $\delta, \Delta \in \ZZ_{> 0}$.
    For later use we also set $\varepsilon$ to be the largest divisor of $N - 1$ that is coprime to $6$.

    \begin{itemize}
        \item
        $M_2(\Gamma_0(N))$ is the ($(g + 1)$-dimensional) $\CC$-vector space of \emph{modular forms} of weight $2$ and level $\Gamma_0(N)$.
        Every $f \in M_2(\Gamma_0(N))$ has a $q$-expansion
        \[
            f = \sum_{n = 0}^{\infty} a_n(f) \, q^n
        \]
        with $a_n(f) \in \CC$ so that we obtain an inclusion $M_2(\Gamma_0(N)) \subseteq \CC[[q]]$.
        We also have the ($g$-dimensional) $\CC$-subspace $S_2(\Gamma_0(N)) \subseteq M_2(\Gamma_0(N))$ of \emph{cusp forms}, i.e.\ those modular forms $f$ with $a_0(f) = 0$.

        \item
        We have a natural isomorphism
        \[
            M_2(\Gamma_0(N)) \cong H^0(X_0(N)_{\CC}, \Omega_{X_0(N)_{\CC}}(\cusps))
        \]
        restricting to an isomorphism
        \[
            S_2(\Gamma_0(N)) \cong H^0(X_0(N)_{\CC}, \Omega_{X_0(N)_{\CC}}).
        \]
        By the general theory of Jacobians of curves, the space $S_2(\Gamma_0(N))$ can furthermore be identified with the cotangent space $\Cot_0(J_0(N)_{\CC})$.

        \item
        We define $\calM \subseteq M_2(\Gamma_0(N))$ to be the $\ZZ[1/6]$-submodule of those modular forms $f$ such that $a_n(f) \in \ZZ[1/6]$ for all $n$ and similarly we write $\calM^0 \subseteq \calM$ for the submodule of cusp forms.
        In fact $\calM \subseteq M_2(\Gamma_0(N))$ is a $\ZZ[1/6]$-structure, meaning that $\calM \otimes_{\ZZ[1/6]} \CC \cong M_2(\Gamma_0(N))$ (and likewise $\calM^0$ is a $\ZZ[1/6]$-structure for the $\CC$-vector space of cusp forms).

        This notation is different from the one in \cite{emerton}, where $\calM \subseteq M_2(\Gamma_0(N))$ is a $\ZZ$-structure that gives back our $\calM$ after localizing at $6$.

        \item
        We have the \emph{Hecke algebra} $\TT$.
        This is a finite free (commutative) $\ZZ[1/6]$-algebra that is generated by the elements $T_p \in \TT$ for primes $p$.

        \item
        $\TT$ acts faithfully on $\calM$.
        For $f \in \calM$ and a prime $p$ we have $a_1(T_p f) = a_p(f)$.

        \item
        $\calM^0 \subseteq \calM$ is a $\TT$-submodule.
        We define $\TT^0$ as the quotient of $\TT$ acting faithfully on $\calM^0$.
        In other words, $\TT^0$ is the quotient of $\TT$ by the annihilator ideal of $\calM^0$.
    \end{itemize}

    \begin{lemma}[{\cite[Proof of Corollary 3.2]{emerton}}] \label{lem:semisimple}
        The base change $\QQ \otimes_{\ZZ[1/6]} \TT$ is a semisimple $\QQ$-algebra, i.e.\ a finite product of number fields.
    \end{lemma}

    \begin{proposition}[{\cite[Proposition 1.3]{emerton}}] \label{prop:duality}
        We have a \enquote{duality} relation between $\calM$ and $\TT$:
        The map
        \[
            \calM \to \ZZ[1/6], \qquad f \mapsto a_1(f)
        \]
        induces an isomorphism of $\TT$-modules $\calM \cong \Hom_{\ZZ[1/6]}(\TT, \ZZ[1/6])$.
        By our previous comments we see that this isomorphism sends a modular form $f$ to the map given by $T_p \mapsto a_p(f)$.

        In the same way we also obtain an isomorphism of $\TT^0$-modules $\calM^0 \cong \Hom_{\ZZ[1/6]}(\TT^0, \ZZ[1/6])$.
    \end{proposition}

    \begin{itemize}
        \item
        We define the Eisenstein series
        \[
            E = \frac{N - 1}{24} + \sum_{n = 1}^{\infty} \sigma^{(N)}(n) \, q^n \in \calM
        \]
        where $\sigma^{(N)}(n)$ is the sum of divisors of $n$ that are not divisible by $N$ and we also set $\calM^{\Eis} \coloneqq \ZZ[1/6] \cdot E \subseteq \calM$.
        The letter $E$ is now used for denoting elliptic curves as well as the Eisenstein series but this shouldn't lead to confusion anywhere.

        \item
        $E$ is a $\TT$-eigenform so that $\calM^{\Eis} \subseteq \calM$ is in fact a $\TT$-submodule.
        We write $\TT^{\Eis}$ for the quotient of $\TT$ acting faithfully on $\calM^{\Eis}$.
        Then $\TT^{\Eis} = \ZZ[1/6]$ and the projection $\TT \to \TT^{\Eis}$ is given by
        \[
            T_p \mapsto \sigma^{(N)}(p) =
            \begin{cases}
                p + 1 & \text{if $p \neq N$,} \\
                1 & \text{if $p = N$.}
            \end{cases}
        \]

        \item
        We have a short exact sequence
        \[
            0 \to \calM^0 \to \calM \to \ZZ[1/6] \to 0
        \]
        where the second map is given by $f \mapsto a_0(f)$.
        Consequently we also obtain a short exact sequence
        \[
            0 \to \calM^{\Eis} \oplus \calM^{0} \to \calM \to \ZZ/\varepsilon \ZZ \to 0,
        \]
        where we recall that $\varepsilon$ is the largest divisor of $N-1$ that is prime to $6$.
        Dualizing this shows that the cokernel of the inclusion $\TT \to \TT^{\Eis} \times \TT^0$ (considered as a map of $\ZZ[1/6]$-modules) is isomorphic to $\ZZ/\varepsilon \ZZ$ as well.

        \item
        Tensoring the short exact sequence of $\TT$-modules
        \[
            0 \to \TT \to \TT^{\Eis} \oplus \TT^0 \to \ZZ/\varepsilon\ZZ \to 0
        \]
        over $\TT$ with $\TT^{\Eis}$ shows that we have $\TT^{\Eis} \otimes_{\TT} \TT^0 \cong \ZZ/\varepsilon\ZZ$.

        Geometrically we thus have the following situation:
        $\Spec(\TT^{\Eis}), \Spec(\TT^0) \subseteq \Spec(\TT)$ are closed subschemes whose scheme-theoretic union is $\Spec(\TT)$ and their intersection is isomorphic to $\Spec(\ZZ/\varepsilon \ZZ)$.
    \end{itemize}

    \begin{definition}
        A maximal ideal $\mm \subseteq \TT$ is called \emph{Eisenstein} if it is contained in $\Spec(\TT^{\Eis}) \cap \Spec(\TT^0)$.
        This is equivalent to saying that $\mm$ is generated by the elements $T_p - \sigma^{(N)}(p)$ and a prime number $\ell$ dividing $\varepsilon$.
    \end{definition}

    We have the following structural result on the Hecke modules $\calM$ and $\calM^0$:

    \begin{theorem}[{\cite[Theorem 1.10 and Proposition 1.14]{emerton} and \cite[Propositions 14.2 and 16.1 and Lemma 15.1]{mazur}}]
        $\calM$ is an invertible $\TT$-module.
        Similarly also $\calM^{0}$ is an invertible $\TT^0$-module.
    \end{theorem}

    \begin{proof}[Idea of proof]
        Using \Cref{prop:duality} and completing we see that it suffices to show that
        \[
            \Hom_{\Zl}(\TT_{\mm}, \Zl)
        \]
        is a finite free $\TT_{\mm}$-module of rank $1$ for all maximal ideals $\mm \subseteq \TT$ of some residue characteristic $\ell$ (which is equivalent to saying that $\TT_{\mm}$ is Gorenstein) and that the same is true for $\TT$ replaced with $\TT^0$.

        Using that $\Spec(\TT_{\mm})$ is a scheme theoretic union of its closed subschemes $\Spec(\TT^{\Eis}_{\mm})$ and $\Spec(\TT^0_{\mm})$ that meet in a common Cartier divisor and a cohomological argument one reduces to just considering the case of $\TT^0$ where one distinguishes between two cases:
        \begin{itemize}
            \item
            When $\mm$ is not Eisenstein then one shows that the $\mm$-torsion $J_0(N)[\mm](\QQbar)$ is a $2$-dimensional $\TT^0/\mm$-vector space.
            From this one then deduces the Gorenstein property of $\TT^0_{\mm}$.

            \item
            When $\mm$ is Eisenstein then one shows that $\TT^0_{\mm}$ is generated by one element as a $\Zl$-algebra.
            Such finite free $\Zl$-algebras are always Gorenstein. \qedhere
        \end{itemize}
    \end{proof}

    \section{\texorpdfstring{The Hecke Module $\calX$}{The Hecke Module X}}

    \begin{itemize}
        \item
        Let $\calS = \curlybr{x_0, \dotsc, x_g} \subseteq X_0(N)(\FNbar)$ be the set of singular points.
        In other words $\calS$ is the set of isomorphism classes of supersingular elliptic curves over $\FNbar$.
        For each $i = 0, \dotsc, g$ fix one supersingular elliptic curve $E_i$ in the isomorphism class corresponding to $x_i$ and write $e_i \coloneqq \abs[\big]{\Aut(E_i)}/2$.

        \item
        For $i, j = 0, \dotsc, g$ we set $L_{i, j} \coloneqq \Hom(E_i, E_j)$.
        This is a free $\ZZ$-module of rank $4$.
        The map
        \[
            L_{i, j} \to \ZZ, \qquad \phi \mapsto \deg(\phi)
        \]
        is a positive definite quadratic form on $L_{i, j}$.
        For $n \geq 0$ we write $r_n(L_{i, j})$ for the number of elements $\phi \in L_{i, j}$ with $\deg(\phi) = n$.
        \begin{itemize}
            \item
            The dual isogeny construction defines an isomorphism of quadratic spaces $L_{i, j} \cong L_{j, i}$.

            \item
            The theta series
            \[
                \Theta(L_{i, j}) \coloneqq \sum_{\phi \in L_{i, j}} q^{\deg(\phi)} = \sum_{n = 0}^{\infty} r_n(L_{i, j}) \, q^n \in \ZZ[[q]]
            \]
            defines an element in $\calM$ (this is referred to as a \enquote{classical theorem} in \cite[Proof of Proposition 3.15]{emerton}).
        \end{itemize}

        \item
        Let $\calX$ be the free $\ZZ[1/6]$-module on the set $\calS$ and let $\calX^0 \subseteq \calX$ be the kernel of the degree map
        \[
            \deg \colon \calX \to \ZZ[1/6].
        \]

        \item
        For a prime $p$ we have the Hecke correspondence
        \[
        \begin{tikzcd}
            &X_0(p N) \arrow[swap]{dl}{\alpha} \arrow{dr}{\beta}
            &
            \\
            X_0(N)
            &
            &X_0(N)
        \end{tikzcd}
        \]
        that is (roughly) given by
        \[
            \alpha \colon (E, C) \mapsto (E, C_N),
        \]
        and
        \[
            \beta \colon (E, C) \mapsto (E/C_p, C/C_p)
        \]
        where $C_p, C_N \subseteq C$ are the \enquote{unique} finite locally free subgroups of order $p$ and $N$ respectively.

        By taking (pre-)images of points along this correspondence we obtain an action of the operator $T_p$ on $\calX$.
        Varying $p$ we get in fact an action of the Hecke algebra $\TT$ on $\calX$ (but this is not obvious).
        Spelling out the definition we see that this action is explicitly given by
        \[
            T_p \, x_i = \sum_{j = 0}^g \frac{r_p(L_{i, j})}{2 e_j} \cdot x_j.
        \]
        The degree of $T_p \, x_i$ is equal to the number of finite subgroups of order $p$ of $E_i$.
        There are $\sigma^{(N)}(p)$ of those subgroups, independently of $i$.
        This shows that $\calX^0 \subseteq \calX$ is a $\TT$-submodule and that the action of $\TT$ on the quotient $\calX/\calX^0 \cong \ZZ$ factors through $\TT^{\Eis}$.

        \item
        The Hecke module $\calX^0$ has the following alternative description:

        The connected component
        \[
            T_0(N) \coloneqq J_0(N)_{\FN}^0 \subseteq J_0(N)_{\FN}
        \]
        of the characteristic $N$ fiber of $J_0(N)$ is a torus and in fact identifies with $\Pic^0(X_0(N)_{\FN})$.
        From the description of $X_0(N)_{\FN}$ is the union of two rational curves that intersect transversely we thus see that we have a natural isomorphism
        \[
            \calX^0 \cong X^*(T_0(N)_{\FNbar})[1/6].
        \]
        Now the Hecke correspondences induce an action of the Hecke algebra on $J_0(N)$ (up to that here we shouldn't invert $6$) so that we also obtain an induced action on the character lattice $X^*(T_0(N)_{\FNbar})$.
        The above isomorphism turns out to be an isomorphism of $\TT$-modules (compare \cite[Proof of Theorem 4.2]{emerton}).
    \end{itemize}

    In the last talk we have shown the following result.

    \begin{theorem}[{\cite[Theorem 3.1]{emerton}}]
        $\calX$ is a faithful $\TT$-module.
        Similarly $\calX^0$ is a faithful $\TT^0$-module (part of this assertion is that the action of $\TT$ on $\calX^0$ factors over $\TT^0$).
    \end{theorem}

    \begin{itemize}
        \item
        Define the element
        \[
            \xx \coloneqq \Delta \sum_{i = 0}^g \frac{x_i}{e_i} \in \calX
        \]
        and set $\calX^{\Eis} \coloneqq \ZZ[1/6] \cdot \xx \subseteq \calX$.
        We can calculate
        \[
            T_p \, \xx = \Delta \sum_{i, j = 0}^g \frac{r_p(L_{i, j})}{2 e_i e_j} x_j = \sigma^{(N)}(p) \xx
        \]
        and see that $\calX^{\Eis} \subseteq \calX$ is a $\TT$-submodule and that the $\TT$-action on $\calX^{\Eis}$ factors through $\TT^{\Eis}$.
    \end{itemize}

    We now have the following result, describing the strutcure of $\calX$ and $\calX^0$ as Hecke modules.

    \begin{theorem}[{\cite[Theorems 4.2 and 4.6]{emerton}}]
        $\calX$ is an invertible $\TT$-module and $\calX^0$ is an invertible $\TT^0$-module.
    \end{theorem}

    \begin{proof}[Idea of proof]
        It suffices to prove the statements after completing at some maximal ideal $\mm$ of $\TT$ respectively $\TT^0$.
        Let us concentrate on the case that $\mm$ is Eisenstein (of residue characteristic $\ell$).

        First of all $\Ql \otimes_{\Zl} \calX$ is free of rank $1$ over $\Ql \otimes_{\Zl} \TT_{\mm}$ (and similarly for $\calX^0$ and $\TT^0$).
        This is because it is a faithful module over a semisimple $\Ql$-algebra (see \Cref{lem:semisimple}) and the $\Ql$-dimensions of the module and the algebra match (they are both equal to $g + 1$).
        Thus it suffices to show that $\calX / \mm \calX$ and $\calX^0 / \mm \calX^0$ are both $1$-dimensional vector spaces over $\TT/\mm \cong \TT^0/\mm \TT^0$.
        Furthermore an explicit calculation shows that $\calX / \mm \calX$ and $\calX^0 / \mm \calX^0$ have the same dimension so that it actually suffices to only consider $\calX^0$.

        Now the torus $T_0(N)$ deforms uniquely to a torus $\calT$ over $\ZN$ and we obtain a natural embedding
        \[
            \calT^{\wedge}_N \to J_0(N)^{\wedge}_N
        \]
        where $(\bullet)^{\wedge}_N$ denotes $N$-completion.
        After passing to the $\ell$-torsion this map algebraizes to give an embedding
        \[
            \calT[\ell] \to J_0(N)_{\ZN}
        \]
        After passing to the generic fiber and using that $\calT[\ell]$ and $\calX^0/\ell \calX^0$ are Cartier dual we obtain the short exact sequence of $\TT^0/\ell$-modules.
        \[
            0 \to \Hom(\calX^0/\ell \calX^0, \mu_{\ell}(\QNbar)) \to J_0(N)[\ell](\QNbar) \to \calX^0 / \ell \calX^0 \to 0.
        \]
        Taking the $\mm$-torsion we get an exact sequence
        \[
            0 \to \Hom(\calX^0/\mm \calX^0, \mu_{\ell}(\QNbar)) \to J_0(N)[\mm](\QNbar) \to (\calX^0/\ell \calX^0)[\mm]
        \]
        of $\TT^0/\mm\TT^0$-vector spaces.
        Now \cite[Corollary 16.3]{mazur} implies that $J_0(N)[\mm]_{\ZN}$ is $2$-dimensional flat $\TT^0/\mm \TT^0$-vector space scheme over $\ZN$ and that its special fiber is not entirely contained in the connected component $T_0(N)$.
        Hence the first term in the exact sequence has to be of dimension $1$ so that also $\calX^0/\mm \calX^0$ is of dimension $1$ as desired.
    \end{proof}

    \section{\texorpdfstring{The Pairing $\calX \otimes_{\TT} \calX \to \calM$}{The Pairing X otimes X to M}}

    \begin{itemize}
        \item
        We first define a perfect symmetric $\ZZ[1/6]$-bilinear pairing $\calX \times \calX \to \ZZ[1/6]$ by setting
        \[
            \anglebr{x_i, x_j} \coloneqq
            \begin{cases}
                e_i = e_j & \text{if $i = j$,}
                \\
                0 & \text{else.}
            \end{cases}
        \]
        Our explicit formula for the action of $\TT$ on $\calX$ and the symmetry $L_{i, j} \cong L_{j, i}$ imply that this pairing is $\TT$-balanced, i.e.\ induces a map of $\ZZ[1/6]$-modules $\calX \otimes_{\TT} \calX \to \ZZ[1/6]$ or equivalently a map of $\TT$-modules
        \[
            \calX \otimes_{\TT} \calX \to \Hom_{\ZZ[1/6]}(\TT, \ZZ[1/6]), \qquad x_i \otimes x_j \mapsto \roundbr[\Big]{T_p \mapsto \anglebr{T_p \, x_i, x_j} = \frac{1}{2} r_p(L_{i, j})}.
        \]

        \item
        Using \Cref{prop:duality} we thus obtain a $\TT$-linear map
        \[
            \theta \colon \calX \otimes_{\TT} \calX \to \calM
        \]
        that is characterized by the property $a_p(\theta(x_i \otimes x_j)) = r_p(L_{i, j})/2$.
        Comparing this with the theta series defined above yields the identity
        \[
            \theta(x_i \otimes x_j) = \frac{1}{2} \Theta(L_{i, j}).
        \]
    \end{itemize}

    \begin{proposition}
        The map $\theta \colon \calX \otimes_{\TT} \calX \to \calM$ is an isomorphism of finite projective $\TT$-modules of rank $1$.
    \end{proposition}

    \begin{proof}
        As was already mentioned, it is clear from the definition that the pairing $\calX \times \calX \to \ZZ[1/6]$ is perfect.
        This means that the induced map of $\TT$-modules $\calX \to \Hom_{\ZZ[1/6]}(\calX, \ZZ[1/6])$ is an isomorphism.
        As $\calX$ is invertible we have natural isomorphisms of $\TT$-modules
        \[
            \Hom_{\ZZ[1/6]}(\calX, \ZZ[1/6]) \cong \Hom_{\TT}(\calX, \Hom_{\ZZ[1/6]}(\TT, \ZZ[1/6])) \cong \calX^{\otimes (-1)} \otimes_{\TT} \Hom_{\ZZ[1/6]}(\TT, \ZZ[1/6]) \cong \calX^{\otimes (-1)} \otimes_{\TT} \calM.
        \]
        Tensoring the isomorphism $\calX \to \calX^{\otimes (-1)} \otimes_{\TT} \calM$ with $\calX$ over $\TT$ gives back our map $\theta$ that thus itself is an isomorphism as desired.
    \end{proof}

    \section{An Example}

    Let us consider the case $N = 11$.
    The reason for picking this prime is that it is the smallest prime $N$ such that $N - 1$ has a prime factor different from $2$ or $3$.

    \begin{itemize}
        \item
        From the genus formula for modular curves it follows that $g = 1$ so that $\TT, \calM, \calX$ are all of rank $2$ over $\ZZ[1/6]$.

        \item
        $\calM^{\Eis}$ is generated by the Eisenstein series
        \[
            E = \frac{5}{12} + q + 3 q^2 + 4 q^3 + 7 q^4 + \dotsb
        \]
        as always.

        \item
        $\calM^0$ is generated by the cusp form
        \[
            f = q \prod_{n = 1}^{\infty} (1 - q^n)^2 (1 - q^{11 n})^2 = q - 2q^2 - q^3 + 2 q^4 + \dotsb.
        \]

        \item
        We have $E \equiv f$ modulo $5$ and $\calM$ is freely generated over $\ZZ[1/6]$ by $E$ and $(E - f)/5$.

        \item
        On the side of the Hecke algebra we have $\TT^{\Eis} \cong \TT^0 \cong \ZZ[1/6]$ and $\TT \subseteq \TT^{\Eis} \times \TT^0$ is given by
        \[
            \TT = \set[\big]{(m_1, m_2)}{\text{$m_1 \equiv m_2$ modulo $5$}}.
        \]
        The element $(E - f)/5 \in \calM$ is a free generator of $\calM$ as a $\TT$-module.
        Indeed we have $E = (5, 0) \cdot (E - f)/5$.

        \item
        There are two isomorphism classes of supersingular curves in characteristic $11$, given by the $j$-invariants $0$ and $1 = 1728$.
        Let $x_0, x_1 \in \calX$ be the corresponding elements.
        Then we have $e_0 = 3$ and $e_1 = 2$.
        Then we get the corresponding element $\xx = 2 x_0 + 3 x_1$ generating $\calX^{\Eis}$ and $x_1 - x_0$ generating $\calX^0$ over $\ZZ[1/6]$ respectively.

        The element
        \[
            x_0 = \frac{1}{5} \xx - \frac{3}{5} (x_1 - x_0) \in \calX
        \]
        then is a free generator of $\calX$ as a $\TT$-module.
        Indeed we have
        \[
            x_1 = \frac{1}{5} \xx + \frac{2}{5} (x_1 - x_0) = (1, -2/3) \cdot x_0.
        \]

        \item
        To compute the pairing $\theta$, it now suffices to compute $\theta(x_0 \otimes x_0)$.
        This is given by
        \[
            \frac{1}{2} \Theta(L_{0, 0}) = \frac{1}{2} + 3 q + \dotsb = \frac{6 E - 9 f}{5} = (6, -9) \cdot \frac{E - f}{5}.
        \]
        As $(6, -9) \in \TT^{\times}$ we thus see that $\theta$ is indeed an isomorphism.
    \end{itemize}

    \printbibliography
\end{document}